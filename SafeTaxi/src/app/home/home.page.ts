import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PostProvider } from 'src/providers/post-provider';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { Platform } from '@ionic/angular';
import { BarcodeScannerOptions, BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";
import { SMS } from '@ionic-native/sms/ngx';
import { ZBar } from '@ionic-native/zbar/ngx';
import { MenuController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { DatosPage } from '../datos/datos.page';
import { LoadingController } from '@ionic/angular';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  zbarOptions:any;
  scannedResult:any;
  showLoader : boolean = true;
  anggota: any;
  name: string;
  qrScan: any;
  customers: any = [];
  limit: number = 13; // LIMIT GET PERDATA
  start: number = 0;
  isLoading = false;
  Dato: string = "";

  codigoqr: string ="";

  scannedCode = null;

  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  navCtrl: any;

  nombre: any;
  primerApellido: string="";
  segundoApellido: string="";
  marca: string="";
  modelo: string="";
  color: string="";
  placa: string="";
  fotografia: string="";
  fotografiaV: any;
  hideMe: boolean=false;
   

  latitud:number
  longitud:number
  total:string
  idConductor: any;
  idVehiculo: any;
  idUsuario: any;

  constructor(private router: Router,
    private postPvdr: PostProvider,
    private menu: MenuController,
    private storage: Storage,
    public toastCtrl: ToastController,
    private dialog: Dialogs,
    private qrScanner: QRScanner,
    private platform: Platform,
    private barcodeScanner: BarcodeScanner,
    private sms: SMS,
    private zbar:ZBar,
    private modalController: ModalController,
    public loadingController: LoadingController,
    private geolocation:Geolocation,
    
  ) {
    //Options

    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };

    this.getGeolocation();


  }

  ionViewWillEnter() {
    this.storage.get('session_storage').then((res) => {
      this.anggota = res;
      this.name = this.anggota.nombres;
      this.idUsuario = this.anggota.idUsuario;
      this.primerApellido = this.anggota.primerApellido;

      console.log(res);
    });

  }




  async prosesLogout() {
    this.storage.clear();
    this.router.navigate(['/login']);
    const toast = await this.toastCtrl.create({
      message: 'Sesion cerrado con Exito',
      duration: 3000
    });
    toast.present();
  }




  scanCode() {
    this.zbar.scan(this.zbarOptions)
   .then(result => {
      console.log(result); // Scanned code
      this.codigoqr=result;
      this.scannedResult = result;
      this.loadCustomer();

   })
   .catch(error => {
      alert(error); // Error message
   });
  }

  async presentModal1() {
    const modal = await this.modalController.create({
      component: DatosPage,
      componentProps: {
        'codigoqr':  this.codigoqr,
      }
    });
    return await modal.present();
  }


  loadCustomer(){
  	return new Promise(resolve => {
  		let body = {
  			aksi : 'getdata',
  			codigoqr: this.codigoqr,
  		};

  		this.postPvdr.postData(body, 'proses-api.php').subscribe(data => {
   if(data.success){
          //this.storage.set('datos_storage',data.result);
          
          for(let item of data.result){
            this.idConductor=item.idConductor;
            this.idVehiculo=item.idVehiculo;
            this.nombre = item.nombres;
            this.primerApellido=item.primerApellido;
             this.segundoApellido=item.segundoApellido;
             this.marca=item.marca;
             this.modelo=item.modelo;
             this.color=item.color;
             this.placa=item.placa;
             this.fotografia=item.fotografia;
             this.fotografiaV=item.fotografiaV;


          }
      //    resolve(true);
          this.presentModal();
      }
      else {
        alert('No hay en la Base de datos');
      }
  			
  		});
  	});
  }

  doRefresh(event){
  	setTimeout(() =>{
  		this.ionViewWillEnter();
  		event.target.complete();
  	}, 500);
  }
  
  async presentModal() {
  

    const modal = await this.modalController.create({
     
            component: DatosPage,
      componentProps: {
        'nombres': this.nombre,
       'primerApellido': this.primerApellido,
        'segundoApellido':this.segundoApellido, 
        'marca' :this.marca,
        'modelo' :this.modelo,
        'color' :this.color,
        'placa' :this.placa,
        'fotografia' :this.fotografia,
        'fotografiaV' :this.fotografiaV,
        'latitud' :this.latitud,
        'longitud' :this.longitud,
        'idVehiculo' :this.idVehiculo,
        'idConductor' :this.idConductor,
        'idUsuario' :this.idUsuario,


      }
    });
    return await modal.present();

  }

  hide() {
    this.hideMe = true;
  }


  getGeolocation(){
 
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitud = resp.coords.latitude
      this.longitud =  resp.coords.longitude
     }).catch((error) => {
       console.log('Error getting location', error);
     });

  }



}
