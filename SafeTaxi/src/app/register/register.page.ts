import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostProvider } from '../../providers/post-provider';

import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  username: string = "";
  password: string = "";
  confirm_password: string = "";
  nombre:string="";
  apellidopaterno:string="";
  apellidomaterno:string="";
  ci:string="";
  fechaNacimiento:Date;
  genero:string="";
  direccion:string="";
  celular:any;
  email:string="";



  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    public toastCtrl: ToastController) { }

  ngOnInit() {
  }
  formLogin() {
    this.router.navigate(['login']);
  }
  async prosesRegister() {
    if (this.username == "") {
      const toast = await this.toastCtrl.create({
        message: 'Nombre de Usuario Requerido',
        duration: 3000
      });
      toast.present();
    } else if (this.password == "") {
      const toast = await this.toastCtrl.create({
        message: 'Password Requerido',
        duration: 3000
      });
      toast.present();
    }
    else if (this.password!=this.confirm_password) {
      const toast = await this.toastCtrl.create({
        message: 'Password no coinciden',
        duration: 3000
      });
      toast.present();
    }
    else { 
    let body = {
      nombre:this.nombre,
      apellidopaterno:this.apellidopaterno,
      apellidomaterno:this.apellidomaterno,
      ci:this.ci,
      fechaNacimiento:this.fechaNacimiento,
      sexo:this.genero,
      direccion:this.direccion,
      celular:this.celular,
      email:this.email,
      login: this.username,
      password: this.password,
      aksi: 'register'
    };
    this.postPvdr.postData(body,'proses-api.php').subscribe(async data => {
      var alertmsg = data.msg;
      if (data.success) {
        this.router.navigate(['/login']);
        const toast = await this.toastCtrl.create({
          message: 'Registro con Exito',
          duration: 3000
        });
        toast.present();
      }
      else {
        const toast = await this.toastCtrl.create({
          message: alertmsg,
          duration: 3000
        });
        toast.present();
      }
    
    });
  }
  }
}
