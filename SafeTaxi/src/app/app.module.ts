import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


import { IonicStorageModule } from '@ionic/storage';
import {HttpModule} from '@angular/http';
import { PostProvider } from '../providers/post-provider';
import { QRScanner } from '@ionic-native/qr-scanner/ngx';
import {Dialogs} from '@ionic-native/dialogs/ngx';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { FormsModule } from '@angular/forms';
import { SMS } from '@ionic-native/sms/ngx';
import { ZBar } from '@ionic-native/zbar/ngx';
import { DatosPage } from './datos/datos.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';



@NgModule({
  declarations: [AppComponent,DatosPage],
  entryComponents: [DatosPage],
  imports: [BrowserModule, 
    HttpModule,
    IonicModule.forRoot(),
     AppRoutingModule,
     FormsModule,
     IonicStorageModule.forRoot()],
     
  providers: [
    StatusBar,
    SplashScreen,
    PostProvider,
    QRScanner,
    Dialogs,
    BarcodeScanner,
    SMS,
    ZBar,
    Geolocation,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
