import { Component, OnInit, Input } from '@angular/core';
import { NavParams, ToastController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { PostProvider } from 'src/providers/post-provider';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.page.html',
  styleUrls: ['./datos.page.scss'],
})
export class DatosPage implements OnInit {
  @Input() codigoqr: string;
  customers: any;
  anggota: any;
  @Input() nombres: string="";
  @Input() primerApellido: string="";
  @Input() segundoApellido: string="";
  @Input() marca: string="";
  @Input() modelo: string="";
  @Input()  color: string="";
  @Input() placa: string="";
  @Input() fotografia: string="";
  @Input() fotografiaV: string="";
  @Input() latitud: number;
  @Input() longitud: number;
  comentario:string="";
  hideMe: boolean=false;
  ocultar: boolean=true;
  calificacion: number=0;
  @Input()  idUsuario: any;
  @Input() idConductor: any;
  @Input()  idVehiculo: any;


  //modalCtrl: any;

  constructor(navParams: NavParams,
    private modalCtrl: ModalController,
    private postPvdr: PostProvider,
    private storage: Storage,
    public toastCtrl: ToastController,
    ) {
    // componentProps can also be accessed at construction time using NavParams
    console.log(navParams.get('nombres'));
  }
  Cancelar() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data

    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  ngOnInit() {
  }

  mostrar() {
    this.hideMe = true;
    this.ocultar=false;
  }


  async prosesRegister() {
    this.hideMe = true;
    this.ocultar=false;
    let body = {

      idUsuario:this.idUsuario,
      idConductor:this.idConductor,
      idVehiculo:this.idVehiculo,
      calificacion:this.calificacion,
      comentario:this.comentario,
      longitud:this.longitud,
      latitud:this.latitud,
      
      aksi: 'registerService'
    };
    this.postPvdr.postData(body,'proses-api.php').subscribe(async data => {
      var alertmsg = data.msg;
      if (data.success) {
        const toast = await this.toastCtrl.create({
          message: 'Servicio Tomado!!',
          duration: 3000
        });
        toast.present();
      }
      else {
      alert(alertmsg);
      }
    
    });
  }
  
  


}
